﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Final_Score : MonoBehaviour {
	private Text _text;
	private Ball_Shoot BS;
	private ScoreBoard Sb;
	// Use this for initialization
	void Start () {
		_text = gameObject.GetComponent <Text> ();
		BS = GameObject.FindObjectOfType <Ball_Shoot> ();
		Sb = GameObject.FindObjectOfType <ScoreBoard> ();
	}
	
	// Update is called once per frame
	void Update () {
		int points = ScoreBoard._score;
		_text.text = points.ToString ();
	}
}
