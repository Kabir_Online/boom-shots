﻿using UnityEngine;
using System.Collections;
using System;

public class Lose_Level : MonoBehaviour {
	private LevelManeger2 levelM;
	private Ball_Shoot ballS;
	private GameObject shooter;
	public GameObject blast;
	// Use this for initialization
	void Start () {
		levelM = GameObject.FindObjectOfType <LevelManeger2> ();
		ballS = GameObject.FindObjectOfType <Ball_Shoot> ();
		shooter = GameObject.Find ("Shooter");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter2D () {
		Instantiate (blast, gameObject.transform.position, Quaternion.identity);
		Invoke ("LevelC", 0.5f);
	}
	void LevelC (){
		levelM.LoadLevel ("start");
}
}