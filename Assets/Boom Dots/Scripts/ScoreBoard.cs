﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreBoard : MonoBehaviour {


	public static int _score;
	public static int highScore;

	private Text text;
	private Ball_Shoot balls;

	// Use this for initialization
	void Start () {
		Reset ();
		text = gameObject.GetComponent <Text> ();
		balls = GameObject.FindObjectOfType <Ball_Shoot> ();

	}

	public static void SetScore(int score) {
		_score = score;
	}

	// Update is called once per frame
	void Update () {
		
		if (highScore < _score) {
			highScore = _score;
		}
	text.text = _score.ToString ();
	}
	void Reset () {
		_score = 0;
	}
}
