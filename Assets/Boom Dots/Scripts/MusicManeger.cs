﻿using UnityEngine;
using System.Collections;

public class MusicManeger : MonoBehaviour {

	public AudioClip [] levelMusicChangeArray;	
	private AudioSource audioSource;

	void Awake () {
		DontDestroyOnLoad(gameObject);
		Debug.Log ("Don't destory on load:" + name);
	}



	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> (); 
	}
	void OnLevelWasLoaded (int level) {
		AudioClip thisLevelMusic = levelMusicChangeArray [level];
		Debug.Log ("Playing clip: " + thisLevelMusic);

		if (thisLevelMusic) {
			audioSource.clip = thisLevelMusic;
			audioSource.loop = true;
			audioSource.Play();
		}

	}	
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ChangeVolume (float volume) {
		audioSource.volume = volume;
	}
}
