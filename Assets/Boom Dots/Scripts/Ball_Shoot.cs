﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
//using UnityEditor.Callbacks;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.Remoting.Messaging;

public class Ball_Shoot : MonoBehaviour {
	public float speed = 1000f;
	public float stop = 0f;
	public Rigidbody2D rb;
	public PolygonCollider2D cir;
	public GameObject tarChild;
	private Collider2D col;

	private GameObject target;
	private GameObject shooter;
	private Ball_Shoot ballS;
	private ScoreBoard scoreB;
	public int score;
	public bool colli = false;


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		target = GameObject.Find ("target");
		shooter = GameObject.Find ("Shooter");
		ballS = GameObject.FindObjectOfType <Ball_Shoot> ();
		scoreB = GameObject.FindObjectOfType <ScoreBoard> ();

		cir = GetComponent<PolygonCollider2D> ();
		score = 0;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)){
			rb.velocity = new Vector2 (0, speed * Time.deltaTime);
		}
	}

	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject == target) {
			Vector3 pos = new Vector3 (0f, -3.5f, 0f);
			this.shooter.transform.position = pos;
			rb.velocity = Vector2.zero;
			rb.isKinematic = false;
			score += 1;
			ScoreBoard.SetScore (score);	
			colli = true;
		}
		colli = false;

	}


	void OnMouseDown (){
		rb.velocity = new Vector2 (0, speed * Time.deltaTime);
	}
	void PerfectHit () {
		score += 3;
	}
}
