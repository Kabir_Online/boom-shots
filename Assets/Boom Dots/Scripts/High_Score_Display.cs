﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class High_Score_Display : MonoBehaviour {
	private Text text;
	// Use this for initialization
	void Start () {
		text = gameObject.GetComponent <Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		int highS = ScoreBoard.highScore;
		text.text = highS.ToString ();
	}
}
