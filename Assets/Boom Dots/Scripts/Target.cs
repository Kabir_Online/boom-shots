﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Xml.XPath;

public class Target : MonoBehaviour {
	public float speed = Mathf.Round (Random.Range (4f, 10f));
	public float width = 2f;
	public GameObject obj;
	public GameObject child;
	public AudioSource audioS;
	public GameObject blast;

	private GameObject Enemy;
	private Animator anim;
	private bool movingRight= true;
	float xmin;
	float xmax;
	private EasingFunction ease;


	// Use this for initialization
	void Start () {
		SpawnEnemies ();
		ease = GameObject.FindObjectOfType <EasingFunction> ();
		anim= gameObject.GetComponentInChildren <Animator>();
		float DistanceToCamera = transform.position.z - Camera.main.transform.position.z;
		Vector3 leftEdge = Camera.main.ViewportToWorldPoint(new Vector3(0,0,DistanceToCamera));
		Vector3 rightEdge = Camera.main.ViewportToWorldPoint(new Vector3(1,0,DistanceToCamera));
		xmin = leftEdge.x;
		xmax = rightEdge.x; 

	}
	void OnTriggerEnter2D (Collider2D col) {
		Instantiate (blast, gameObject.transform.position, Quaternion.identity);
		audioS.Play ();
		anim.Play ("Target_Spawn");
		Invoke ("SpawnEnemies", 0.01f);
	}


	void SpawnEnemies () {
		Vector3 position = new Vector3 (Random.Range (-3f, 3f), Random.Range (-1.5f, 1.3f));
		gameObject.transform.position = position;
	}

	// Update is called once per frame
	void Update () {
		/*
		Debug.Log (xmin);
		Debug.Log (xmax);
		Debug.Log (Time.deltaTime);
		Debug.Log (EasingFunctions.EaseOutQuad (xmax, xmin, (Time.deltaTime % xmax - xmin)));
		*/

		if (movingRight) {
			//transform.position += Vector3.right * speed * EasingFunction.EaseOutQuad (xmax, xmin, Time.deltaTime);
			transform.position += Vector3.right * speed * Time.deltaTime;
		} else {
			transform.position += Vector3.left * speed * Time.deltaTime; 
		}

		float rightEdgeFormation = transform.position.x + (0.5f * width);
		float leftEdgeFormation = transform.position.x - (0.5f * width);
		if (leftEdgeFormation < xmin) {
			movingRight = true;
		}else if (rightEdgeFormation > xmax) {
			movingRight = false;
		}
	}
}
